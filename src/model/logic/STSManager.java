package model.logic;

import API.ISTSManager;
import model.data_structures.*;
import model.vo.*;
import com.google.gson.*;

import java.io.*;
import java.util.Calendar;
import java.util.Iterator;

public class STSManager implements ISTSManager
{
	private DoubleLinkedList<AgencyVO> agencyList;
	private RingList<ZoneVO> stopList;
	private RingList<ViajeVO> tripList;
	private DoubleLinkedList<CalendarVO> calendarList;
	private DoubleLinkedList<CalendarDateVO> calendarDateList;
	private DoubleLinkedList<VORuta> routeList;
	private DoubleLinkedList<StopTimeVO> stopTimeList;
	private Queue<BusUpdateVO> lifoBus;
	private DoubleLinkedList<VOShape> shapeList;
	private Queue<StopEstimVO> lifoStop;

	public STSManager() {
		ITSInit();
	}

	@Override
	public void ITSInit() 
	{
		agencyList = new DoubleLinkedList<>(null, null);
		stopList = new RingList<>(null);
		tripList = new RingList<>(null);
		calendarList = new DoubleLinkedList<>(null, null);
		calendarDateList = new DoubleLinkedList<>(null, null);
		routeList = new DoubleLinkedList<>(null, null);
		stopTimeList = new DoubleLinkedList<>(null, null);
		lifoBus = new Queue<>();
		shapeList= new DoubleLinkedList<>(null, null);
		lifoStop = new Queue<>();
		// TODO Auto-generated method stub
	}

	@Override
	public void ITScargarGTFS() 
	{
		loadAgency();
		loadStop();
		loadTrip();
		loadCalendar();
		loadCalendarDate();
		loadRoutes();
		loadStopTime();
		loadShapes();

		File f = new File("data/json/buses");
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length; i++) {
			readBusUpdate(updateFiles[i]);
		}

		File f1 = new File("data/json/stops");
		File[] updateFiles1 = f1.listFiles();
		for (int i = 0; i < updateFiles1.length; i++) {
			readStopUpdate(updateFiles[i]);
		}

	}

	private void loadAgency() {
		File file = new File("data/txt/agency.txt");
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			int i = 0;
			try {
				while ((line = br.readLine()) != null) {
					if (i > 0) {
						String[] tabLine = line.split(",");
						AgencyVO a = new AgencyVO(tabLine[0], tabLine[1], tabLine[2], tabLine[3]);
						Node<AgencyVO> newNode = new Node<>(a, null, null);
						this.agencyList.addAtEnd(newNode);
					}
					i++;
				}
				br.close();
			} catch (IOException e) {
				System.out.print("End file");
			}
		} catch (FileNotFoundException e) {
				System.out.print("File not found");
		}
		System.out.println("agency.txt has been successfully loaded");
	}

	private void loadShapes() {
		File file = new File("data/txt/shapes.txt");
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			int i = 0;
			try {
				while ((line = br.readLine()) != null) {
					if (i > 0) {
						String[] tabLine = line.split(",");
						VOShape a = new VOShape(Integer.parseInt(tabLine[0]), Integer.parseInt(tabLine[1]), Integer.parseInt(tabLine[2]), Integer.parseInt(tabLine[3]),Integer.parseInt(tabLine[4]));
						Node<VOShape> newNode = new Node<>(a, null, null);
						this.shapeList.addAtEnd(newNode);
					}
					i++;
				}
				br.close();
			} catch (IOException e) {
				System.out.print("End file");
			}
		} catch (FileNotFoundException e) {
				System.out.print("File not found");
		}
		System.out.println("shapes.txt has been successfully loaded");
	}
	private void loadStop() {
		File file = new File("data/txt/stops.txt");
		try{
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			int i =0;
			try{
				while ((line = br.readLine()) != null) {
					if (i>0) {
						String[] tabLine = line.split(",");
						String zoneId = tabLine[6];
						ZoneVO zoneNode = null;
						Iterator<ZoneVO> it = stopList.iterator();
						while (it.hasNext()){
							zoneNode = it.next();
							if (zoneNode.getZoneId().equals(zoneId))
								break;
						}
						if (zoneNode == null){
							zoneNode = new ZoneVO(zoneId);
							stopList.addInOrder(new Node<>(zoneNode, null, null));
						}
						DoubleLinkedList<StopVO> list = zoneNode.getListStop();
						StopVO stop = new StopVO(Integer.parseInt(tabLine[0]), Integer.parseInt(tabLine[1]), tabLine[2], tabLine[3], tabLine[6]);
						Node<StopVO> stopNode = new Node<>(stop, null, null);
						list.addInOrder(stopNode);
					}
					i++;
				}
				br.close();
			} catch (IOException e){
				System.out.print("End file");
			}
		} catch (FileNotFoundException e){
			System.out.print("Unknown file");
		}
		System.out.println("stops.txt has been successfully loaded");
	}

	private void loadTrip() {
		File file = new File("data/txt/trips.txt");
		try{
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			int i =0;
			try{
				while ((line = br.readLine()) != null) {
					if (i>0) {
						String[] tabLine = line.split(",");
						int routeId = Integer.parseInt(tabLine[0]);
						ViajeVO viajeNode = null;
						Iterator<ViajeVO> it = tripList.iterator();
						while (it.hasNext()){
							viajeNode = it.next();
							if (viajeNode.getRouteId() == routeId)
								break;
						}
						if (viajeNode == null){
							// if null, viaje object does not exist, we create the node
							viajeNode = new ViajeVO(routeId);
							tripList.addInOrder(new Node<>(viajeNode, null, null));
						}
						DoubleLinkedList<TripVO> list = viajeNode.getListViaje();
						TripVO trip = new TripVO(Integer.parseInt(tabLine[0]),
								Integer.parseInt(tabLine[1]),
								Integer.parseInt(tabLine[2]),
								tabLine[3],
								Integer.parseInt(tabLine[5]),
								Integer.parseInt(tabLine[6]),
								Integer.parseInt(tabLine[7]));
						Node<TripVO> tripNode = new Node<>(trip, null, null);
						list.addInOrder(tripNode);
					}
					i++;
				}
				br.close();
			} catch (IOException e){
				System.out.print("End file");
			}
		} catch (FileNotFoundException e){
			System.out.print("Unknown file");
		}
		System.out.println("trips.txt has been successfully loaded");
	}

	private void loadCalendar() {
		File file = new File("data/txt/calendar.txt");
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			int i = 0;
			try {
				while ((line = br.readLine()) != null) {
					if (i > 0) {
						String[] tabLine = line.split(",");

						CalendarVO a = new CalendarVO(Integer.parseInt(tabLine[0]),
								Integer.parseInt(tabLine[1]),
								Integer.parseInt(tabLine[2]),
								Integer.parseInt(tabLine[3]),
								Integer.parseInt(tabLine[4]),
								Integer.parseInt(tabLine[5]),
								Integer.parseInt(tabLine[6]),
								Integer.parseInt(tabLine[7]),
								tabLine[8],
								tabLine[9]);

						Node<CalendarVO> newNode = new Node<>(a, null, null);
						this.calendarList.addAtEnd(newNode);
					}
					i++;
				}
				br.close();
			} catch (IOException e) {
				System.out.print("End file");
			}
		} catch (FileNotFoundException e) {
				System.out.print("File not found");
		}
		System.out.println("calendar.txt has been successfully loaded");
	}
	private void loadCalendarDate() {
		File file = new File("data/txt/calendar.txt");
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			int i = 0;
			try {
				while ((line = br.readLine()) != null) {
					if (i > 0) {
						String[] tabLine = line.split(",");

						CalendarDateVO a = new CalendarDateVO(Integer.parseInt(tabLine[0]),
								tabLine[1],
								Integer.parseInt(tabLine[2]));

						Node<CalendarDateVO> newNode = new Node<>(a, null, null);
						this.calendarDateList.addAtEnd(newNode);
					}
					i++;
				}
				br.close();
			} catch (IOException e) {
				System.out.print("End file");
			}
		} catch (FileNotFoundException e) {
				System.out.print("File not found");
		}
		System.out.println("calendar_date.txt has been successfully loaded");
	}

	private void loadRoutes() {
		File file = new File("data/txt/routes.txt");
		try{
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			int i =0;
			try{
				while ((line = br.readLine()) != null) {
					if (i>0) {
						String[] tabLine = line.split(",");
						VORuta r = new VORuta(tabLine[0], tabLine[2]);
						Node<VORuta> newNode = new Node<>(r, null, null);
						this.routeList.addAtEnd(newNode);
					}
					i++;
				}
				br.close();
			} catch (IOException e) {
				System.out.print("End file");
			}
		} catch (FileNotFoundException e) {
			System.out.print("File not found");
		}
		System.out.println("routes.txt has been successfully loaded");
	}

	private void loadStopTime() {
		File file = new File("data/txt/stop_times.txt");
		try{
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			int i =0;
			try{
				while ((line = br.readLine()) != null) {
					if (i>0) {
						String[] tabLine = line.split(",");
						StopTimeVO r = new StopTimeVO (Integer.parseInt(tabLine[0]), tabLine[1], tabLine[2], Integer.parseInt(tabLine[3]));
						Node<StopTimeVO> newNode = new Node<>(r, null, null);
						this.stopTimeList.addAtEnd(newNode);
					}
					i++;
				}
				br.close();
			} catch (IOException e) {
				System.out.print("End file");
			}
		} catch (FileNotFoundException e) {
			System.out.print("File not found");
		}
		System.out.println("stop_times.txt has been successfully loaded");
	}

	private void readBusUpdate(File rtFile) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(rtFile));
			Gson gson = new GsonBuilder().create();
			BusUpdateVO[] busUpdateArray = gson.fromJson(reader, BusUpdateVO[].class);

			for (BusUpdateVO busUpdate: busUpdateArray) {
				lifoBus.enqueue(busUpdate);
			}

		} catch (FileNotFoundException ex) {
			System.out.println("File "+ rtFile.toString()+" not found");
		}
		System.out.println("Buses json files have been successfully loaded");
	}

	private void readStopUpdate(File rtFile) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(rtFile));
			Gson gson = new GsonBuilder().create();
			StopEstimVO[] stopUpdateArray = gson.fromJson(reader, StopEstimVO[].class);

			for (StopEstimVO stopUpdate: stopUpdateArray) {
				lifoStop.enqueue(stopUpdate);
			}

		} catch (FileNotFoundException ex) {
			System.out.println("File "+ rtFile.toString()+" not found");
		}
		System.out.println("Stops json files have been successfully loaded");
	}

	@Override
	public void ITScargarTR(String fecha) 
	{
		// TODO Auto-generated method stub	
	}

	@Override
	public IList<VORuta> ITSrutasPorEmpresa(String nombreEmpresa, String fecha)
	{
		String empresaId="";
		Iterator<AgencyVO> it = agencyList.iterator();
		while (it.hasNext()){
			AgencyVO agency = it.next();
			if (agency.getName().compareTo(nombreEmpresa)==0)
				empresaId = agency.getAgencyId();
		}
		if (empresaId.compareTo("")==0){
			System.out.println("The firm is not known");
			return null;
		}
		DoubleLinkedList<VORuta> listRoad = new DoubleLinkedList<>(null, null);
		Iterator<CalendarVO> itCalendar = calendarList.iterator();
		while (itCalendar.hasNext()){
			CalendarVO calendar = itCalendar.next();
			if (calendar.getStartDate().compareTo(fecha)<0 && calendar.getEndDate().compareTo(fecha)>0){
				int serviceId = calendar.getServiceID();
				int routeId = getRouteId(serviceId);
				Node<VORuta> node = null;
				Iterator<VORuta> itRoute = routeList.iterator();
				while (itRoute.hasNext() && node ==null){
					VORuta r = itRoute.next();
					if (r.getAgencyId().compareTo(empresaId)==0){
						VORuta ruta = new VORuta(r.getIdRoute(),r.getShortName());
						node = new Node<>(ruta, null, null);
						listRoad.addInOrder(node);
					}
				}
			}
		}
		return listRoad;
	}

	private int getRouteId(int serviceId){
		Iterator<ViajeVO> itList = tripList.iterator();
		int routeId = -1;
		while (itList.hasNext()){
			DoubleLinkedList<TripVO> list = itList.next().getListViaje();
			Iterator<TripVO> it = list.iterator();
			while (it.hasNext() && routeId == -1){
				TripVO obj = it.next();
				if (obj.getServiceId() == serviceId)
					routeId = obj.getRouteId();
			}
		}
		return routeId;

	}
	private TripVO getRoute(int serviceId){
		Iterator<ViajeVO> itList = tripList.iterator();
		TripVO route =null;
		int routeId = -1;
		while (itList.hasNext()){
			DoubleLinkedList<TripVO> list = itList.next().getListViaje();
			Iterator<TripVO> it = list.iterator();
			while (it.hasNext() && routeId == -1){
				TripVO obj = it.next();
				if (obj.getServiceId() == serviceId)
					{route = obj;}
			}}return route;}


	@Override
	public IList<VOViaje> ITSviajesRetrasadosRuta(String idRuta, String fecha) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOParada> ITSparadasRetrasadasFecha(String fecha) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOTransfer> ITStransbordosRuta(String idRuta, String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VOPlan ITSrutasPlanUtilizacion(IList<String> idsDeParadas, String fecha, String horaInicio, String horaFin) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VORuta> ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha)

	{String empresaId="";
	Iterator<AgencyVO> it = agencyList.iterator();
	while (it.hasNext()){
		AgencyVO agency = it.next();
		if (agency.getName().compareTo(nombreEmpresa)==0)
			empresaId = agency.getAgencyId();
	}
	if (empresaId.compareTo("")==0){
		System.out.println("no valido");
		return null;
	}
	DoubleLinkedList<VORuta> listaRutas = new DoubleLinkedList<>(null, null);
	Iterator<CalendarVO> itCalendar = calendarList.iterator();
	while (itCalendar.hasNext()){
		CalendarVO calendar = itCalendar.next();
		if (calendar.getStartDate().compareTo(fecha)<0 && calendar.getEndDate().compareTo(fecha)>0){
			int serviceId = calendar.getServiceID();

			Node<VORuta> node = null;
			Iterator<VORuta> itRoute = routeList.iterator();
			while (itRoute.hasNext() && node ==null){
				VORuta r = itRoute.next();
				if (r.getAgencyId().compareTo(empresaId)==0){
					VORuta ruta = new VORuta(r.getIdRoute(),r.getShortName());
					int paradas=0;
					for (int i = 0; i < tripList.size(); i++) {
						if(tripList.getElementAtK(i).getRouteId()+""==r.getIdRoute())
						{for (int j = 0; j < tripList.getElementAtK(i).getListViaje().size(); j++) {
							for (int j2 = 0; j2 < stopTimeList.size(); j2++) {
								if(stopTimeList.getElementAtK(j2).getTripId()==tripList.getElementAtK(i).getListViaje().getElementAtK(j).getTripId()&&tripList.getElementAtK(i).getListViaje().getElementAtK(j).getServiceId()==serviceId)
								{
									paradas++;
								}

						}


					}}

					ruta.setParadas(paradas);
					node = new Node<>(ruta, null, null);
					if(ruta.getParadas()!=0){
					listaRutas.addInOrder2(node);}
				}
			}
		}
	}
	}return listaRutas;}



	@Override
	public IList<VOViaje> ITSviajesRetrasoTotalRuta(String idRuta, String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VORangoHora ITSretardoHoraRuta(String idRuta, String Fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOViaje> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaInicio,
			String horaFin) 
	{
		DoubleLinkedList<VOViaje> resp= new DoubleLinkedList<VOViaje>(null, null);
		for (int i = 0; i < stopTimeList.size(); i++) {
			{
				if(stopTimeList.getElementAtK(i).getStopId()+""==idOrigen&&stopTimeList.getElementAtK(i).getDepartureTime().compareTo(horaInicio)>0)
				{
					for (int j = i++; j < stopTimeList.size(); j++)
					{
						if(stopTimeList.getElementAtK(i).getTripId()==stopTimeList.getElementAtK(j).getTripId()&&stopTimeList.getElementAtK(i).getStopId()+""==idDestino&&stopTimeList.getElementAtK(j).getArrivalTime().compareTo(horaFin)<0)
						{
							VOViaje viaje =new VOViaje(stopTimeList.getElementAtK(i).getTripId()+"");
							viaje.setArrival(stopTimeList.getElementAtK(i).getDepartureTime());
							viaje.setSalida(stopTimeList.getElementAtK(j).getArrivalTime());
							for (int k = 0; k < tripList.size(); k++) {
								for (int k2 = 0; k2 < tripList.getElementAtK(k).getListViaje().size(); k2++) {
									if (tripList.getElementAtK(k).getListViaje().getElementAtK(k2).getTripId()+""==viaje.getIdViaje()){
										viaje.setRouteId(tripList.getElementAtK(k).getRouteId()+"");
									}

								}


							}

							Node<VOViaje> node = new Node<>(viaje, null, null);
							resp.add(node);

						}


					}


				}




			}


	}
		// TODO Auto-generated method stub
		return resp;
	}

	@Override
	public VORuta ITSrutaMenorRetardo(String fecha) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOServicio> ITSserviciosMayorDistancia(String fecha)
	{IList<VOServicio> resp = new DoubleLinkedList<>(null, null);
	Iterator<CalendarVO> itCalendar = calendarList.iterator();
	while (itCalendar.hasNext()){
		CalendarVO calendar = itCalendar.next();
		if (calendar.getStartDate().compareTo(fecha)<0 && calendar.getEndDate().compareTo(fecha)>0){
			int serviceId = calendar.getServiceID();
			VOServicio obj= new VOServicio();
	         Node<VOServicio> ser1 = new Node<>(obj, null, null);
				for (int i = 0; i < tripList.size(); i++) {
					for (int j = 0; j < tripList.getElementAtK(i).getListViaje().size(); j++) {
						if(tripList.getElementAtK(i).getListViaje().getElementAtK(j).getServiceId()==serviceId)
						{
							for (int j2 = 0; j2 < shapeList.size(); j2++) {
								if(tripList.getElementAtK(i).getListViaje().getElementAtK(j).getShapeId()==shapeList.getElementAtK(j2).getShapeid())
								{
									if(shapeList.getElementAtK(j2).getShapeid()!=shapeList.getElementAtK(j2++).getShapeid())
									{

											ser1.getObj().setDistanciaRecorrida(ser1.getObj().getDistanciaRecorrida()+shapeList.getElementAtK(j2).getDist());
									}
								}
							}
						}

					}
					((DoubleLinkedList<VOServicio>) resp).addInOrder2(ser1);
						ser1= new Node<>(obj, null, null);
				}




				}}
	return resp;}

	@Override
	public IList<VORetardo> ITSretardosViaje(String fecha, String idViaje) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOParada> ITSparadasCompartidas(String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
