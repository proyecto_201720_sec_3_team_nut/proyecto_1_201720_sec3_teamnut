package model.vo;

public class VOShape implements Comparable<VOShape>{
	
private int shapeid;
private int shaplat;
private int shaplon;
private int seq;
private int dist;

public VOShape(int shapeid, int shaplat, int shaplon, int seq, int dist) {
	super();
	this.shapeid = shapeid;
	this.shaplat = shaplat;
	this.shaplon = shaplon;
	this.seq = seq;
	this.dist = dist;
}
public int getShapeid() {
	return shapeid;
}
public void setShapeid(int shapeid) {
	this.shapeid = shapeid;
}
public int getShaplat() {
	return shaplat;
}
public void setShaplat(int shaplat) {
	this.shaplat = shaplat;
}
public int getShaplon() {
	return shaplon;
}
public void setShaplon(int shaplon) {
	this.shaplon = shaplon;
}
public int getSeq() {
	return seq;
}
public void setSeq(int seq) {
	this.seq = seq;
}
public int getDist() {
	return dist;
}
public void setDist(int dist) {
	this.dist = dist;
}
@Override
public int compareTo(VOShape o) {
	if (this.shapeid > o.getShapeid())
		return 1;
	else if (this.shapeid < o.getShapeid())
		return -1;
	else
		return 0;
}


}
