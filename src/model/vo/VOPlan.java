package model.vo;

import model.data_structures.IList;

/**
 * Estructura para modelar lista req 5A
 */
public class VOPlan 
{
	//Atributos

	/**
	 * Modela las paradas en el plan
	 */
	private IList<ParadaPlanVO> secuenciaDeParadas;	

	//M�todos
	/**
	 * @return the secuenciaDeParadas
	 */
	public IList<ParadaPlanVO> getSecuenciaDeParadas() 
	{
		return secuenciaDeParadas;
	}

	/**
	 * @param secuenciaDeParadas the secuenciaDeParadas to set
	 */
	public void setSecuenciaDeParadas(IList<ParadaPlanVO> secuenciaDeParadas)
	{
		this.secuenciaDeParadas = secuenciaDeParadas;
	}

	//Nested classes
	/**
	 * Nested class, modela una parada, con sus rutas y viajes asociados
	 */
	public class ParadaPlanVO implements Comparable<ParadaPlanVO>
	{
		//Atributos

		/**
		 * Modela el id de la parada
		 */
		private String idParada;

		/**
		 * Modela las rutas que usan la parada
		 */
		private IList<RutaPlanVO> rutasAsociadasAParada;

		/**
		 * Estructura para modelar una ruta que usa la parada
		 */

		//M�todos
		/**
		 * @return the idParada
		 */
		public String getIdParada() 
		{
			return idParada;
		}

		/**
		 * @param idParada the idParada to set
		 */
		public void setIdParada(String idParada) 
		{
			this.idParada = idParada;
		}

		/**
		 * @return the rutasAsociadasAParada
		 */
		public IList<RutaPlanVO> getRutasAsociadasAParada()
		{
			return rutasAsociadasAParada;
		}

		/**
		 * @param rutasAsociadasAParada the rutasAsociadasAParada to set
		 */
		public void setRutasAsociadasAParada(IList<RutaPlanVO> rutasAsociadasAParada)
		{
			this.rutasAsociadasAParada = rutasAsociadasAParada;
		}

		@Override
		public int compareTo(ParadaPlanVO o) {
			return this.idParada.compareTo(o.getIdParada());
		}

		//Nested class
		public class RutaPlanVO implements Comparable<RutaPlanVO>
		{
			/**
			 * Modela el id de la ruta
			 */
			private String idRuta;

			/**
			 * Modela los viajes en la ruta asociados a la parada
			 */
			private IList<ViajePlanVO> viajesEnRuta;

			//Mtodos

			/**
			 * @return the idRuta
			 */
			public String getIdRuta() 
			{
				return idRuta;
			}

			/**
			 * @param idRuta the idRuta to set
			 */
			public void setIdRuta(String idRuta)
			{
				this.idRuta = idRuta;
			}

			/**
			 * @return the viajesEnRuta
			 */
			public IList<ViajePlanVO> getViajesEnRuta() 
			{
				return viajesEnRuta;
			}

			/**
			 * @param viajesEnRuta the viajesEnRuta to set
			 */
			public void setViajesEnRuta(IList<ViajePlanVO> viajesEnRuta)
			{
				this.viajesEnRuta = viajesEnRuta;
			}

			@Override
			public int compareTo(RutaPlanVO o) {
				return this.idRuta.compareTo(o.getIdRuta());
			}

			//Nested class
			/**
			 * Estructura para modelar viaje en plan
			 */
			public class ViajePlanVO implements Comparable<ViajePlanVO>
			{
				//Atributos

				/**
				 * Modela la hora de parada en la parada asociada
				 */
				private String horaDeParada;

				/**
				 * Modela el id del viaje
				 */
				private String idViaje;

				//Mtodos

				/**
				 * @return the horaDeParada
				 */
				public String getHoraDeParada() 
				{
					return horaDeParada;
				}

				/**
				 * @param horaDeParada the horaDeParada to set
				 */
				public void setHoraDeParada(String horaDeParada)
				{
					this.horaDeParada = horaDeParada;
				}

				/**
				 * @return the idViaje
				 */
				public String getIdViaje()
				{
					return idViaje;
				}

				/**
				 * @param idViaje the idViaje to set
				 */
				public void setIdViaje(String idViaje) 
				{
					this.idViaje = idViaje;
				}

				@Override
				public int compareTo(ViajePlanVO o) {
					return this.idViaje.compareTo(getIdViaje());
				}
			}
		}
	}
}
