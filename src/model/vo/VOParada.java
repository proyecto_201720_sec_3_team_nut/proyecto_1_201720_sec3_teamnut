package model.vo;

public class VOParada implements Comparable<VOParada>
{
	//Atributos
	/**
	 * Modela el id de la parada
	 */
	private String stopId;
	
	/**
	 * Modela el n�mero de incidentes
	 */
	private int numeroIncidentes;
	
	//Mtodos
	/**
	 * @return the stopId
	 */
	public String getStopId()
	{
		return stopId;
	}

	/**
	 * @param stopId the stopId to set
	 */
	public void setStopId(String stopId) 
	{
		this.stopId = stopId;
	}

	/**
	 * @return the numeroIncidentes
	 */
	public int getNumeroIncidentes()
	{
		return numeroIncidentes;
	}

	/**
	 * @param numeroIncidentes the numeroIncidentes to set
	 */
	public void setNumeroIncidentes(int numeroIncidentes)
	{
		this.numeroIncidentes = numeroIncidentes;
	}

	@Override
	public int compareTo(VOParada o) {
		return stopId.compareTo(o.getStopId());
	}
}
