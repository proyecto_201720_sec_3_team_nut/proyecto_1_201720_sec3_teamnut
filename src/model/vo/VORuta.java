package model.vo;

public class VORuta implements Comparable<VORuta>
{
	//Atributos
	
	/**
	 * Modela el id de la ruta
	 */
	 private String idRoute;

	 /**
	  * Model el short name de la ruta
	  */
	 private String shortName;

	private String agencyId;
	
	private int paradas;

	public int getParadas() {
		return paradas;
	}

	public void setParadas(int paradas) {
		this.paradas = paradas;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public VORuta(String idRoute, String shortName) {
		this.idRoute = idRoute;
		this.shortName = shortName;
	}
	//Mtodos
	 
	/**
	 * @return the idRoute
	 */
	public String getIdRoute() 
	{
		return idRoute;
	}

	/**
	 * @param idRoute the idRoute to set
	 */
	public void setIdRoute(String idRoute) 
	{
		this.idRoute = idRoute;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() 
	{
		return shortName;
	}

	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName)
	{
		this.shortName = shortName;
	}

	@Override
	public int compareTo(VORuta o) {
		return this.idRoute.compareTo(o.getIdRoute());
		
	}
	 public int compareTo2(RouteVO o) {
	        if (paradas>o.getParadas())
	            return 1;
	        else if (paradas< o.getParadas())
	            return -1;
	        else
	            return 0;
	    }
}
