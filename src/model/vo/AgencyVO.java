package model.vo;

/**
 * Created by Johan on 22/09/2017.
 */
public class AgencyVO implements Comparable<AgencyVO>{

    private String agencyId;
    private String name;
    private String url;
    private String lang;

    public AgencyVO(String id, String n, String u, String l){
        agencyId = id;
        name = n;
        url = u;
        lang = l;
    }

    public String getAgencyId() {
        return agencyId;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getLang() {
        return lang;
    }

    @Override
    public int compareTo(AgencyVO o) {
        return agencyId.compareTo(o.getAgencyId());
    }
}
