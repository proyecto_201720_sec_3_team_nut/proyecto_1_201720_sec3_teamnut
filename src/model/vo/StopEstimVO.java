package model.vo;

import java.util.List;

/**
 * Created by Johan on 24/09/2017.
 */
public class StopEstimVO implements Comparable<StopEstimVO>{


    private int RouteNo;
    private String RouteName;
    private String Direction;

    public int getRouteNo() {
        return RouteNo;
    }

    public String getRouteName() {
        return RouteName;
    }

    public String getDirection() {
        return Direction;
    }

    public StopEstimVO.RouteMap getRouteMap() {
        return RouteMap;
    }

    public List<StopEstimVO.Schedules> getSchedules() {
        return Schedules;
    }

    private RouteMap RouteMap;

    public StopEstimVO(int routeNo, String routeName, String direction, StopEstimVO.RouteMap routeMap, List<StopEstimVO.Schedules> schedules) {
        RouteNo = routeNo;
        RouteName = routeName;
        Direction = direction;
        RouteMap = routeMap;
        Schedules = schedules;
    }

    private List<Schedules> Schedules;

    @Override
    public int compareTo(StopEstimVO o) {
        if (RouteNo > o.getRouteNo())
            return 1;
        else if (RouteNo < o.getRouteNo())
            return -1;
        else
            return 0;
    }


    public class RouteMap {
        private String Href;

        public String getHref() {
            return Href;
        }

        public RouteMap(String href) {

            Href = href;
        }
    }

    public class Schedules {
        private String Pattern;
        private String Destination;
        private String ExpectedLeaveTime;
        private int ExpectedCountdown;
        private String ScheduleStatus;
        private boolean CancelledTrip;
        private boolean CancelledStop;
        private boolean AddedTrip;
        private boolean AddedStop;
        private String LastUpdate;

        public String getPattern() {
            return Pattern;
        }

        public String getDestination() {
            return Destination;
        }

        public String getExpectedLeaveTime() {
            return ExpectedLeaveTime;
        }

        public int getExpectedCountdown() {
            return ExpectedCountdown;
        }

        public String getScheduleStatus() {
            return ScheduleStatus;
        }

        public boolean isCancelledTrip() {
            return CancelledTrip;
        }

        public boolean isCancelledStop() {
            return CancelledStop;
        }

        public boolean isAddedTrip() {
            return AddedTrip;
        }

        public boolean isAddedStop() {
            return AddedStop;
        }

        public String getLastUpdate() {
            return LastUpdate;
        }

        public Schedules(String pattern, String destination, String expectedLeaveTime, int expectedCountdown, String scheduleStatus, boolean cancelledTrip, boolean cancelledStop, boolean addedTrip, boolean addedStop, String lastUpdate) {

            Pattern = pattern;
            Destination = destination;
            ExpectedLeaveTime = expectedLeaveTime;
            ExpectedCountdown = expectedCountdown;
            ScheduleStatus = scheduleStatus;
            CancelledTrip = cancelledTrip;
            CancelledStop = cancelledStop;
            AddedTrip = addedTrip;
            AddedStop = addedStop;
            LastUpdate = lastUpdate;
        }
    }
}
