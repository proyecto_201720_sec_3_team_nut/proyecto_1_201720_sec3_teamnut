package model.vo;

/**
 * Created by Johan on 22/09/2017.
 */
public class StopTimeVO implements Comparable<StopTimeVO>{

    private int tripId;
    private String arrivalTime;
    private String departureTime;
    private int stopId;

    public int getTripId() {
        return tripId;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public int getStopId() {
        return stopId;
    }

    public StopTimeVO(int tripId, String arrivalTime, String departureTime, int stopId) {

        this.tripId = tripId;
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
        this.stopId = stopId;
    }

    @Override
    public int compareTo(StopTimeVO o) {
        if (tripId > o.getTripId())
            return 1;
        else if (tripId < o.getTripId())
            return -1;
        else
            return 0;
    }
}
