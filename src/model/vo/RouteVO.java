package model.vo;

/**
 * Created by Johan on 22/09/2017.
 */
public class RouteVO implements Comparable<RouteVO>{

    private int routeId;
    private String agencyId;
    private String longName;
    private int paradas;

    public int getParadas() {
		return paradas;
	}

	public void setParadas(int paradas) {
		this.paradas = paradas;
	}

	public int getRouteId() {
        return routeId;
    }

    public String getAgencyId() {
        return agencyId;
    }

    public String getLongName() {
        return longName;
    }

    public RouteVO(int routeId, String agencyId, String longName) {

        this.routeId = routeId;
        this.agencyId = agencyId;
        this.longName = longName;
    }

    @Override
    public int compareTo(RouteVO o) {
        if (routeId>o.getRouteId())
            return 1;
        else if (routeId< o.getRouteId())
            return -1;
        else
            return 0;
    }
   
}
