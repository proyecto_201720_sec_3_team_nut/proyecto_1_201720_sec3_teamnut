package model.vo;

/**
 * Created by Johan on 22/09/2017.
 */
public class TripVO implements Comparable<TripVO>{

    private int routeId;
    private int serviceId;
    private int tripId;
    private String headsign;
    private int directionId;
    private int blockId;
    private int shapeId;

    public TripVO(int r, int se, int t, String h, int d, int b, int s){
        routeId =r;
        serviceId=se;
        tripId=t;
        headsign=h;
        directionId=d;
        blockId=b;
        shapeId=s;
    }

    public int getRouteId() {
        return routeId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public int getTripId() {
        return tripId;
    }

    public String getHeadsign() {
        return headsign;
    }

    public int getDirectionId() {
        return directionId;
    }

    public int getBlockId() {
        return blockId;
    }

    public int getShapeId() {
        return shapeId;
    }

    @Override
    public int compareTo(TripVO o) {
        if (routeId > o.getRouteId())
            return 1;
        else if (routeId < o.getRouteId())
            return -1;
        else
            return 0;
    }
}
