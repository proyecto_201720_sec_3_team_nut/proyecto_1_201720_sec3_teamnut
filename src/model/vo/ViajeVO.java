package model.vo;

import model.data_structures.*;

/**
 * Representation of a Viaje object
 */
public class ViajeVO implements Comparable<ViajeVO> {

    private int routeId;
    private DoubleLinkedList<TripVO> listViaje;

    public ViajeVO(int rId){
        this.routeId = rId;
        this.listViaje = new DoubleLinkedList<TripVO>(null,null);
    }

    public int getRouteId() {
        return routeId;
    }

    public DoubleLinkedList<TripVO> getListViaje() {
        return listViaje;
    }

    @Override
    public int compareTo( ViajeVO o) {
        if (this.routeId == o.getRouteId())
            return 0;
        else if (this.routeId> o.getRouteId())
            return 1;
        else
            return -1;
    }


}
