package model.vo;

import model.data_structures.IList;

public class VOTransfer implements Comparable<VOTransfer>
{
	/**
	 * Modela el tiempo de transbordo
	 */
	private int transferTime;
	
	/**
	 * Lista de paradas que conforman el transbordo
	 */
	private IList<VOParada> listadeParadas;

	/**
	 * @return the transferTime
	 */
	public int getTransferTime()
	{
		return transferTime;
	}

	/**
	 * @param transferTime the transferTime to set
	 */
	public void setTransferTime(int transferTime) 
	{
		this.transferTime = transferTime;
	}

	/**
	 * @return the listadeParadas
	 */
	public IList<VOParada> getListadeParadas()
	{
		return listadeParadas;
	}

	/**
	 * @param listadeParadas the listadeParadas to set
	 */
	public void setListadeParadas(IList<VOParada> listadeParadas) 
	{
		this.listadeParadas = listadeParadas;
	}


	@Override
	public int compareTo(VOTransfer o) {
		if (this.transferTime>o.getTransferTime())
			return 1;
		else if (this.transferTime<o.getTransferTime())
			return -1;
		else
			return 0;
	}
}
