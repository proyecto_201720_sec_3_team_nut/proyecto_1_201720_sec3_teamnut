package model.vo;

/**
 * Created by Johan on 22/09/2017.
 */
public class CalendarVO implements Comparable<CalendarVO> {

    private int serviceID;
    private int monday;
    private int tuesday;
    private int wednesday;
    private int thursday;
    private int friday;
    private int saturday;
    private int sunday;
    private String startDate;
    private String endDate;

    public CalendarVO(int id, int m, int tu, int w, int th, int f, int sa, int su, String start, String end){
        serviceID=id;
        monday=m;
        tuesday=tu;
        wednesday=w;
        thursday=th;
        friday= f;
        saturday=sa;
        sunday=su;
        startDate=start;
        endDate=end;
    }

    public int getServiceID() {
        return serviceID;
    }

    public int getMonday() {
        return monday;
    }

    public int getTuesday() {
        return tuesday;
    }

    public int getWednesday() {
        return wednesday;
    }

    public int getThursday() {
        return thursday;
    }

    public int getFriday() {
        return friday;
    }

    public int getSaturday() {
        return saturday;
    }

    public int getSunday() {
        return sunday;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    @Override
    public int compareTo(CalendarVO o) {
        if (serviceID>o.getServiceID())
            return 1;
        else if (serviceID< o.getServiceID())
            return -1;
        else
            return 0;
    }
}
