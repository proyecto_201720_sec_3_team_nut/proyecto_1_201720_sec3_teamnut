package model.vo;

/**
 * Created by Johan on 22/09/2017.
 */
public class CalendarDateVO implements Comparable<CalendarDateVO> {

    private int serviceId;
    private String date;
    private int exceptionType;

    public CalendarDateVO(int id, String d, int e){
        serviceId=id;
        date=d;
        exceptionType=e;
    }

    public int getServiceId() {
        return serviceId;
    }

    public String getDate() {
        return date;
    }

    public int getExceptionType() {
        return exceptionType;
    }

    @Override
    public int compareTo(CalendarDateVO o) {
        if (serviceId>o.getServiceId())
            return 1;
        else if (serviceId< o.getServiceId())
            return -1;
        else
            return 0;
    }
}
