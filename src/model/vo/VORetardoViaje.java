package model.vo;

public class VORetardoViaje implements  Comparable<VORetardoViaje>{
	
	private int viajeid;
	private int tiempoRetardo;
	
	public int getViajeid() {
		return viajeid;
	}
	public void setViajeid(int viajeid) {
		this.viajeid = viajeid;
	}
	public int getTiempoRetardo() {
		return tiempoRetardo;
	}
	public void setTiempoRetardo(int tiempoRetardo) {
		this.tiempoRetardo = tiempoRetardo;
	}


	@Override
	public int compareTo(VORetardoViaje o) {
		if (this.tiempoRetardo > o.getTiempoRetardo())
			return 1;
		else if (this.tiempoRetardo < o.getTiempoRetardo())
			return -1;
		else
			return 0;
	}
}
