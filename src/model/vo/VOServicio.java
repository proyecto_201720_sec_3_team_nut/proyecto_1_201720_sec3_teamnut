package model.vo;

public class VOServicio implements Comparable<VOServicio>
{
	/**
	 * Modela el id del servicio
	 */
     private String serviceId;

     /**
      * Modela la sistancia recorrida del servicio dado una fecha (req 2C)
      */
     private int distanciaRecorrida;
     
	/**
	 * @return the serviceId
	 */
	public String getServiceId() 
	{
		return serviceId;
	}

	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(String serviceId) 
	{
		this.serviceId = serviceId;
	}

	/**
	 * @return the distanciaRecorrida
	 */
	public int getDistanciaRecorrida()
	{
		return distanciaRecorrida;
	}

	/**
	 * @param distanciaRecorrida the distanciaRecorrida to set
	 */
	public void setDistanciaRecorrida(int distanciaRecorrida)
	{
		this.distanciaRecorrida = distanciaRecorrida;
	}

	@Override
	public int compareTo(VOServicio o) {
		return this.serviceId.compareTo(o.getServiceId());
	}
	public int compareTo2(VOServicio o) {
		return this.distanciaRecorrida-o.getDistanciaRecorrida();
	}
}
