package model.vo;

//import sun.security.krb5.internal.crypto.Des;
import java.util.Date;

public class BusUpdateVO implements Comparable<BusUpdateVO>{

    private int VehicleNo;
    private int TripId;
    private int RouteNo;
    private String Direction; //Create enum?
    private String Destination;
    private String Pattern;
    private float Latitude;
    private float Longitude;
    private Date RecordedTime;
    private StopEstimVO.RouteMap RouteMap;

    public StopEstimVO.RouteMap getRouteMap() {
        return RouteMap;
    }

    public BusUpdateVO(int vNb, int t, int rNb, String d, String dest, String p, float lat, float lon, Date r, StopEstimVO.RouteMap ro){
        VehicleNo = vNb;
        TripId = t;
        RouteNo = rNb;
        Direction = d;
        Destination = dest;
        Pattern = p;
        Latitude = lat;
        Longitude = lon;
        RecordedTime = r;
        RouteMap = ro;
    }

    public int getTripId() {
        return TripId;
    }

    public int getRouteNo() {
        return RouteNo;
    }

    public String getDirection() {
        return Direction;
    }

    public String getDestination() {
        return Destination;
    }

    public String getPattern() {
        return Pattern;
    }

    public float getLatitude() {
        return Latitude;
    }

    public float getLongitude() {
        return Longitude;
    }

    public Date getRecordedTime() {
        return RecordedTime;
    }

    public int getVehicleNo() {

        return VehicleNo;
    }

    @Override
    public int compareTo(BusUpdateVO o) {
        if (TripId > o.getTripId())
            return 1;
        else if (TripId < o.getTripId())
            return -1;
        else
            return 0;
    }
}
