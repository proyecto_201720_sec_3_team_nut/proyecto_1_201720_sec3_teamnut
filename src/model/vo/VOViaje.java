package model.vo;

import model.data_structures.DoubleLinkedList;

public class VOViaje implements Comparable<VOViaje>
{
	/**
	 * Modela el id del viaje
	 */
	private String idViaje;

	private DoubleLinkedList<VORetardo> retardoList;
	
    private	String arrival;
    private String salida;
    public String getArrival() {
		return arrival;
	}


	public void setArrival(String arrival) {
		this.arrival = arrival;
	}


	public String getSalida() {
		return salida;
	}


	public void setSalida(String salida) {
		this.salida = salida;
	}


	public String getRouteId() {
		return routeId;
	}


	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}


	private String routeId;

	public VOViaje(String idViaje) {
		this.idViaje = idViaje;
		
	}

	
	public DoubleLinkedList<VORetardo> getRetardoList() {

		return retardoList;
	}

	/**
	 * @return the idViaje
	 */
	public String getIdViaje()
	{
		return idViaje;
	}

	/**
	 * @param idViaje the idViaje to set
	 */
	public void setIdViaje(String idViaje)
	{
		this.idViaje = idViaje;
	}


	public int compareTo( VOViaje o) {
        
            return idViaje.compareTo(o.getIdViaje());
    }

	
}
