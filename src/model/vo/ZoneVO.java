package model.vo;

import model.data_structures.*;

/**
 * Representation of a Zone object
 */
public class ZoneVO implements Comparable<ZoneVO> {

    private String zoneId;
    private DoubleLinkedList<StopVO> listStop;

    public ZoneVO(String z){
        this.zoneId = z;
        this.listStop = new DoubleLinkedList<StopVO>(null,null);
    }

    public String getZoneId() {
        return zoneId;
    }

    public DoubleLinkedList<StopVO> getListStop() {
        return listStop;
    }

    @Override
    public int compareTo(ZoneVO o) {
        return this.zoneId.compareTo(o.getZoneId());
    }
}
