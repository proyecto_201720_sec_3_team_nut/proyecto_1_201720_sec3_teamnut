package model.vo;

/**
 * Created by Johan on 22/09/2017.
 */
public class StopVO implements Comparable<StopVO>{

    private int stopId;
    private int stopCode;
    private String name;
    private String desc;
    private String zoneId;

    public StopVO(int id, int c, String n, String d, String z){
        stopId = id;
        stopCode = c;
        name = n;
        desc = d;
        zoneId = z;
    }

    @Override
    public int compareTo(StopVO o) {
        return zoneId.compareTo(o.getZoneId());
    }

    public int getStopId() {
        return stopId;
    }

    public int getStopCode() {
        return stopCode;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public String getZoneId() {
        return zoneId;
    }
}
