package model.data_structures;

public class Stack<E extends Comparable<E>> implements IStack<E> {

	private DoubleLinkedList<E> list;
	
	public Stack() {
		this.list = new DoubleLinkedList<>(null, null);
	}
	
	@Override
	public void push(E item) {
		Node<E> node = new Node<>(item, null, null);
		this.list.addAtEnd(node);
	}

	@Override
	public E pop() {
		if (this.list.size() != 0) {
			Node<E> node = this.list.getLast();
			this.list.setLast(node.getPrevious());
			return node.getObj();	
		}
		return null;
	}

}
