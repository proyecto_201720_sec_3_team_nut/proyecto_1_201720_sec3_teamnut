package model.data_structures;

public class Node<T extends Comparable<T>> implements Comparable<Node<T>>{
	
	private T obj;
	private Node<T> next;
	private Node<T> previous;
	
	public Node(T obj, Node<T> nxt, Node<T> pvs) {
		this.obj = obj;
		this.next = nxt;
		this.previous = pvs;
	}
	
	public T getObj() {
		return this.obj;
	}
	
	public Node<T> getNext(){
		return this.next;
	}
	
	public void setNext(Node<T> nxt) {
		this.next = nxt;
	}
	
	public Node<T> getPrevious(){
		return this.previous;
	}
	
	public void setPrevious(Node<T> pvs) {
		this.previous = pvs;
	}

	@Override
	public int compareTo(Node<T> o) {
		return this.obj.compareTo(o.getObj());
	}

	public int compareTo2(Node<T> actual) {
		// TODO Auto-generated method stub
		return this.obj.compareTo(actual.getObj());}	
	}


