package model.data_structures;

public interface IList<T extends Comparable<T>> extends Iterable<T> {

	int size();
	
	void add(Node<T> obj);
	void addAtEnd(Node<T> obj);
	void addAtK(Node<T> obj, int k);
	void addInOrder(Node<T> obj);
	
	T getElement();
	T getElementAtK(int k);
	
	void delete();
	void deleteAtK(int k);
	
	Node<T> next();
	Node<T> previous();
}
