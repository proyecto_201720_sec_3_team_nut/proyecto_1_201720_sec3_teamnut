package model.data_structures;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class RingList<T extends Comparable <T>> implements IList<T> {

	private Node<T> actual;
	private Node<T> start;

	public RingList(Node<T> node){
		this.start = node;
		this.actual = node;
	}

	@Override
	public Iterator<T> iterator() {
		Iterator<T> it = new Iterator<T>() {
			int index = 0;

			@Override
			public boolean hasNext() {
				return index < size();
			}

			@Override
			public T next() {
				if (!hasNext())
					throw new NoSuchElementException();
				index++;
				return getElementAtK(index);
			}

			@Override
			public void remove() {
				return;
			}

		};
		return it;
	}

	@Override
	public int size() {
		if (this.start == null)
			return 0;
		else if (this.actual == this.actual.getNext())
			return 1;
		else {
			int size = 2;
			Node<T> node = this.start.getNext();
			while (node != this.start){
				size++;
				node = node.getNext();
			}
			return size;
		}
	}

	/**
	 * add element after the current element, if the ring list is initially empty, the current node is the element added
	 * @param obj element to add
	 */
	@Override
	public void add(Node<T> obj) {
		switch (this.size()){
			case 0:
				this.start = obj;
				obj.setNext(obj);
				obj.setPrevious(obj);
				this.actual = obj;
				break;
			case 1:
				obj.setNext(this.start);
				obj.setPrevious(this.start);
				this.start.setNext(obj);
				this.start.setPrevious(obj);
				this.start = obj;
				break;
			default:
				obj.setNext(this.actual.getNext());
				obj.setPrevious(this.actual);
				this.actual.getNext().setPrevious(obj);
				this.actual.setNext(obj);
		}
	}

	@Override
	public void addAtEnd(Node<T> obj) {
		if (this.size() <= 1)
			this.add(obj);
		else {
			obj.setNext(this.start);
			obj.setPrevious(this.start.getPrevious());
			this.start.getPrevious().setNext(obj);
			this.start.setPrevious(obj);
		}
	}

	@Override
	public void addAtK(Node<T> obj, int k) {
		if (k >= this.size())
			this.addAtEnd(obj);
		else {
			Node<T> current = this.start;
			int i = 0;
			while (i!=k){
				current = current.getNext();
				i++;
			}
			obj.setNext(current);
			obj.setPrevious(current.getPrevious());
			current.setPrevious(obj);
			if (current.getPrevious() != null)
				current.getPrevious().setNext(obj);
		}

	}

	@Override
	public void addInOrder(Node<T> obj){
		if (this.size()==0)
			add(obj);
		else {
			int counter = 0;
			while((counter < this.size())&& (obj.compareTo(this.actual)>0)){
				counter++;
			}
			this.addAtK(obj, counter);
		}
	}

	@Override
	public T getElementAtK(int K) {
		int k = K % (this.size());
		int index = 0;
		Node<T> current = this.start;
		while (index != k){
			current = current.getNext();
			index++;
		}
		return current.getObj();
	}

	@Override
	public T getElement() {
		return this.actual.getObj();
	}

	@Override
	public void delete() {
		this.actual.getPrevious().setNext(this.actual.getNext());
		this.actual.getNext().setPrevious(this.actual.getPrevious());
		this.actual = this.actual.getNext();
	}

	@Override
	public void deleteAtK(int K) {
		int k = K % (this.size()-1);
		Node<T> current = this.start;
		Node<T> pvs = current.getPrevious();
		int i = 0;
		while (i != k){
			pvs = current;
			current = current.getNext();
			i++;
		}
		pvs.setNext(current.getNext());
		current.getNext().setPrevious(pvs);
		// need to free current
	}

	@Override
	public Node<T> next() {
		return this.actual.getNext();
	}

	@Override
	public Node<T> previous() {
		return this.actual.getPrevious();
	}

}
