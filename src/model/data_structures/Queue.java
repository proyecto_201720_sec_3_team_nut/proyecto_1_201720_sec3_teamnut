package model.data_structures;

public class Queue<E extends Comparable<E>> implements IQueue<E> {
	
	private DoubleLinkedList<E> list;
	
	public Queue() {
		this.list = new DoubleLinkedList<>(null, null);
	}

	@Override
	public void enqueue(E item) {
		Node<E> node = new Node<>(item, null, null);
		this.list.addAtEnd(node);
	}

	@Override
	public E dequeue() {
		if (this.list.size() != 0) {
			Node<E> node = this.list.getFirst();
			this.list.setFirst(node.getNext());
			return node.getObj();
		}
		return null;
	}

}
