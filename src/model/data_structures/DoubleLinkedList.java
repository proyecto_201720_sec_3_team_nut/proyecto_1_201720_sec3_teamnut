package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DoubleLinkedList<T extends Comparable<T>> implements IList<T> {

	private Node<T> actual;
	private Node<T> first;
	private Node<T> last;
	
	public DoubleLinkedList(Node<T> f, Node<T> l) {
		this.setActual(f);
		this.first = f;
		this.last = l;
	}
	
	public Node<T> getFirst(){
		return this.first;
	}
	
	public Node<T> getLast(){
		return this.last;
	}
	
	public void setFirst(Node<T> first) {
		this.first = first;
	}

	public void setLast(Node<T> last) {
		this.last = last;
	}

	@Override
	public Iterator<T> iterator() {
		Iterator<T> it = new Iterator<T>() {
			int index = 0;

			@Override
			public boolean hasNext() {
				return index < size();
			}

			@Override
			public T next() {
				if (!hasNext())
					throw new NoSuchElementException();
				index++;
				return getElementAtK(index);
			}

			@Override
			public void remove() {
				return;
			}

		};
		return it;
	}

	@Override
	public int size() {
		Node<T> node = this.first;
		int size = 0;
		while (node!=null) {
			size++;
			node = node.getNext();
		}
		return size;
	}

	@Override
	public void add(Node<T> obj) {
		if (this.first == null) {
			this.first = obj;
			this.last = obj;
			this.setActual(obj);
			obj.setNext(null);
			obj.setPrevious(null);
		} else if (this.getActual() == this.last) {
			this.addAtEnd(obj);
		} else {
			this.getActual().getNext().setPrevious(obj);
			obj.setNext(this.getActual().getNext());
			this.getActual().setNext(obj);
			obj.setPrevious(this.getActual());
		}
	}

	@Override
	public void addAtEnd(Node<T> obj) {
		if (this.first == null)
			this.add(obj);
		else {
			obj.setNext(null);
			obj.setPrevious(this.last);
			this.last.setNext(obj);
			this.last = obj;
		}

	}

	@Override
	public void addAtK(Node<T> obj, int k) {
		if (k >= this.size())
			this.addAtEnd(obj);
		else {
			Node<T> current = this.first;
			int index = 0;
			while (index != k) {
				current = current.getNext();
				index++;
			}
			obj.setNext(current);
			obj.setPrevious(current.getPrevious());
			if (current.getPrevious() != null)
				current.getPrevious().setNext(obj);
			current.setPrevious(obj);
			if (index == 0)
				this.first = obj;
		}
	}

	@Override
	public void addInOrder(Node<T> obj) {
		if (this.size() == 0)
			add(obj);
		else {
			int counter = 0;
			while ((counter < this.size()) && (obj.compareTo(this.getActual())>0)) {
				counter++;
			}
			this.addAtK(obj, counter);
		}
	}
	public void addInOrder2(Node<T> obj) {
		if (this.size() == 0)
			add(obj);
		else {
			int counter = 0;
			while ((counter < this.size()) && (obj.compareTo2(this.getActual())>0)) {
				counter++;
			}
			this.addAtK(obj, counter);
		}
	}

	@Override
	public T getElement() {
		return this.getActual().getObj();
	}

	@Override
	public T getElementAtK(int k) {
		Node<T> current = this.first;
		int index = 0;
		while (index != k) {
			current = current.getNext();
			index++;
		}
		return current.getObj();
	}

	@Override
	public void delete() {
		if (this.size() > 1) {
			if (this.first == this.getActual()){
				this.getActual().getNext().setPrevious(null);
				this.first = this.getActual().getNext();
				this.setActual(this.first);
			} else if (this.getActual() == this.last) {
				this.getActual().getPrevious().setNext(null);
				this.last = this.getActual().getPrevious();
				this.setActual(this.last);
			} else {
				this.getActual().getPrevious().setNext(this.getActual().getNext());
				this.getActual().getNext().setPrevious(this.getActual().getPrevious());
				this.setActual(this.getActual().getNext());
			}
		} else {
			this.setActual(null);
			this.first = null;
			this.last = null;
		}
	}

	@Override
	public void deleteAtK(int k) {
		if (k <= this.size() && this.size() > 0) {
			Node<T> current = this.first;
			int index = 0;
			while (index != k) {
				current = current.getNext();
				index++;
			}
			if (index == 0) {
				this.first = this.first.getNext();
				this.first.setPrevious(null);
			} else if (index == this.size()) {
				this.last = this.last.getPrevious();
				this.last.setNext(null);
			} else {
				current.getPrevious().setNext(current.getNext());
				current.getNext().setPrevious(current.getPrevious());
			}
		}
	}

	@Override
	public Node<T> next() {
		return this.getActual().getNext();
	}

	@Override
	public Node<T> previous() {
		return this.getActual().getPrevious();
	}

	public Node<T> getActual() {
		return actual;
	}

	public void setActual(Node<T> actual) {
		this.actual = actual;
	}

}
