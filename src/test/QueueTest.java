package test;

import junit.framework.TestCase;
import model.data_structures.Node;
import model.data_structures.Queue;;


public class QueueTest extends TestCase{
	private Queue list;
	private Node a;
	private Node b;
	private Node c;
	private Node d;
	private Node e;
	private Node f;
	private Node g;
	private Node h;
	
	
	private void setup()
	{ 
		a =new Node("1", null, null);
		b =new Node("2", null, null);
		c =new Node("3", null, null);
		d =new Node("4", null, null);
		e =new Node("5", null, null);
		f =new Node("6", null, null);
		g =new Node("7", null, null);
		h =new Node("8", null, null);
		
		list = new Queue();
		
	}
	
	public void testpush( )
    {
        setup( );
        list.enqueue(a);
        list.enqueue(b);
        list.enqueue(c);
        list.enqueue(d);
       assertEquals( "Error al hacer push", a.toString(), list.dequeue().toString() );
    }
 public void testpop( )
    {setup( );
    list.enqueue(a);
    list.enqueue(b);
    list.enqueue(c);
    list.enqueue(d);
    list.enqueue(e);
   assertEquals( "Error al hacer push", a.toString(), list.dequeue().toString() );
   assertEquals( "Error al hacer push", b.toString(), list.dequeue().toString() );
   assertEquals( "Error al hacer push", c.toString(), list.dequeue().toString() );
   assertEquals( "Error al hacer push", d.toString(), list.dequeue().toString() );
   assertEquals( "Error al hacer push", e.toString(), list.dequeue().toString() );

    }
	
}
