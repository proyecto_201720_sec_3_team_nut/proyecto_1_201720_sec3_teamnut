package test;
import model.data_structures.*;
import junit.framework.TestCase;
public class DoubleLinkedListTest extends TestCase{

	private DoubleLinkedList list;
	private Node a;
	private Node b;
	private Node c;
	private Node d;
	private Node e;
	private Node f;
	private Node g;
	private Node h;
	
	
	private void setup()
	{ 
		a =new Node("1", null, null);
		b =new Node("2", null, null);
		c =new Node("3", null, null);
		d =new Node("4", null, null);
		e =new Node("5", null, null);
		f =new Node("6", null, null);
		g =new Node("7", null, null);
		h =new Node("8", null, null);
		
		list = new DoubleLinkedList<>(a, a);
		
	}
	public void testam()
	{
		 
		 setup( );
		 list.addAtEnd(b);
		 list.addAtEnd(c);
		 list.addAtEnd(d);
		 list.addAtEnd(e);
		 list.addAtEnd(f);
		
		 assertEquals( "Error al dar tama�o", 6,list.size());
		
	}
	 public void testelimatk()
	 {
		 
		 setup( );
		 list.addAtEnd(b);
		 list.addAtEnd(c);
		 list.addAtEnd(d);
		 list.addAtEnd(e);
		 list.addAtEnd(f);
		 list.deleteAtK(4);
	       assertEquals( "Error al eliminar", "6",list.getElementAtK(4).toString());
	       
	 }
	 public void testgetatk()
	 {
		 
		 setup( );
		 list.addAtEnd(b);
		 list.addAtEnd(c);
		 list.addAtEnd(d);
		 list.addAtEnd(e);
		 list.addAtEnd(f);
	       assertEquals( "Error al preguntar", "5", list.getElementAtK(4).toString());
	       assertEquals( "Error al preguntar", "1", list.getElementAtK(0).toString());
	 }
	 public void testagregar( )
	    {
	        setup( );
	        list.addAtEnd(b);
	        list.addAtEnd(c);
	        list.addAtEnd(d);
	       assertEquals( "Error al agregar", 4, list.size() );
	      
	    }
	 
	
	 
	 
	 
	
	
}
