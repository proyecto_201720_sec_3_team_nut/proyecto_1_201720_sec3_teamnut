package test;
import model.data_structures.Stack;
import junit.framework.TestCase;
import model.data_structures.Node;
import model.data_structures.RingList;
public class StackTest extends TestCase{
	private Stack list;
	private Node a;
	private Node b;
	private Node c;
	private Node d;
	private Node e;
	private Node f;
	private Node g;
	private Node h;
	
	
	private void setup()
	{ 
		a =new Node("1", null, null);
		b =new Node("2", null, null);
		c =new Node("3", null, null);
		d =new Node("4", null, null);
		e =new Node("5", null, null);
		f =new Node("6", null, null);
		g =new Node("7", null, null);
		h =new Node("8", null, null);
		
		list = new Stack();
		
	}
	
	public void testpush( )
    {
        setup( );
        list.push(a);
        list.push(b);
        list.push(c);
        list.push(d);
       assertEquals( "Error al hacer push", d.toString(), list.pop().toString() );
    }
 public void testpop( )
    {setup( );
    list.push(a);
    list.push(b);
    list.push(c);
    list.push(d);
   assertEquals( "Error al hacer push", d.toString(), list.pop().toString() );
   assertEquals( "Error al hacer push", c.toString(), list.pop().toString() );
   assertEquals( "Error al hacer push", b.toString(), list.pop().toString() );
   assertEquals( "Error al hacer push", a.toString(), list.pop().toString() );

    }
	
	
}
